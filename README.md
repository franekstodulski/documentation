# Documentation

## Git workflow

### Before starting new branch

### Git-land
Download `git land` and configure it.
`https://github.com/git-land/git-land`

Inside of project run these commands:

*	 `git config --add remote.origin.fetch '+refs/pull/*/head:refs/remotes/origin/pr/*`
*	 `git config git-land.remote origin`
*	 `git config git-land.target stable`

# Starting new feature branch:

* `git pull origin stable` - fetch latest changes from stable branch
* `git checkout -b <ticketID> ` - create feature branch from stable

After finishing your work:

* `git add .`
* `git commit -m "<ticketID>[<status>] <Ticket title>"` - ex. `git commit -m "#97ngw[completed] Add new API for trainers"`
* `git push origin <branchName>`

After pushing to origin, create pull request with `stable` as target branch. Add at least one person as reviewer.

__Code Review__


* _rejeceted_ :

	* `git chcekout stable`
	* `git pull origin stable`
	* `git checkout <featureBranch>`
	* `git rebase -i stable`
	* pick all of them, resolve all conflicts, and: 
		
		* `git add .`
		* `git rebase --continue`
		
	* add your changes, fix accordingly to reviewer's comments
	* `git add .` 
	
	Case 1:
	
	
	* `git commit --amend` (to save changes under previous commit message)  
	* `git push origin -f <branchName>` (override git log history)
	
	Case 2 (before merging with TEST squash all commits to one)
	
	
	* `git commit -m "<Commit message>"` (create new Commit) 
	* `git push origin <branchName>` 
	

* _approved_: 

	* `git chcekout stable`
	* `git pull origin stable`
	* `git checkout <featureBranch>`
	* `git rebase -i stable`
	* pick all of them, resolve all conflicts, and: 
		
		* `git add .`
		* `git rebase --continue`
	* `git checkout test`
	* `git pull origin test`
	* `git merge <featureBranch>`
	* `git push origin test`
	
	
__Test__ :


* _rejected_ :

	
	Pull stable -> Rebase `<featureBranch`> with stable -> Fix
	
	* If change is small -> merge with test, and push again
	* If change is bigger -> commit, push and create pull request
	
* _approved_ :

	
	Pull stable -> Rebase `<featureBranch`> with stable
	
	
	* checkout on feature branch
	* `git land <featureBranch>`
	
	
	
	__WARINING!!!__
	
	Check if origin is __STABLE__
	
	`Are you sure you want to merge #97dut into origin/stable?`
	
	### Congrats!! You've just deployed your feature!

		



